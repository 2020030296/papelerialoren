const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');
const db = require('../models/articulo');
const us = require('../models/usuarios')
const session = require('express-session');

router.use(session({
    secret:'12345',
    resave: false,
    saveUninitialized: true
}));
router.get('/', (req,res)=>{
 const usuario = req.session.username;
 
 if (usuario) {
    const valores={
        encabezado:"Papeleria loren",
            titulo:"Inicio",
            usuario: usuario,
            link: ''
    }
    res.render('inicio.html', valores)
 } else {
    const valores={
        encabezado:"Papeleria loren",
            titulo:"Inicio",
            usuario: 'inicia sesion',
            link: 'href=/login'
    }
    res.render('inicio.html', valores)
 }
    

    
});
router.get('/login', (req,res)=>{


    res.render('login.html')
})

router.get('/registro', (req,res)=>{


    res.render('registro.html')
})
router.get('/productos', async(req,res)=>{
    const usuario = req.session.username
    if (usuario) {
        var user = usuario;
        var link = ''
    } else {
        var user = 'inicia sesion'
        var link = 'href=/login'
    }
    try{
        const resultado = await db.mostrarTodos();
        const valores={
            encabezado:"Nuestros Articulos",
                titulo:"Articulos",
                articulos:resultado,
                usuario: user,
                link:link
        }
        res.render('productos.html', valores)
    }catch(error){
        console.error(error);
    }
    
});

router.get('/info', (req,res)=>{

    const usuario = req.session.username;
 
 if (usuario) {
    const valores={
        encabezado:"¿Quienes Somos?",
            titulo:"Nosotros",
            usuario: usuario,
            link: ''
    }
    res.render('info.html', valores)
 } else {
    const valores={
        encabezado:"¿Quienes Somos?",
            titulo:"Nosotros",
            usuario: 'inicia sesion',
            link: 'href=/login'
    }
    res.render('info.html', valores)
 }
})
router.get('/contacto', (req,res)=>{

    const valores={
        encabezado:"Contactanos",
            titulo:"Contactos"
    }

    res.render('contactos.html', valores)
})


module.exports=router;