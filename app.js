const http = require('http');
const express= require('express');
const bodyParser= require('body-parser');
const app = express();
const misRutas = require ("./router/index");
const path = require ("path");
const cookie = require('cookie-parser');


app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({extended:true}));
app.set('view engine', 'ejs');
app.use(misRutas);
app.use(express.json());
app.engine('html', require('ejs').renderFile);


const puerto = 3001;
app.listen(puerto,()=>{
    console.log('iniciando puerto ' + puerto);
})














